#!/bin/bash
set -e
umask 022
if [[ "$1" == "--help" ]] ; then
    echo "$0 [options]"
    echo "  --help         : Write this message"
    echo "  -c             : Run command from subsystem"
    echo "  --no-install   : Do not install if not installed."
    echo "  --update       : Install update if exists."
    echo "  --system       : Change target subsystem."
    echo "  --hostctl      : Use hostctl daemon."
    echo "  --fork         : Create fork process."
    echo "  --umount       : Umount subsystem."
    echo "  --kill         : Kill running process under subsystem"
    echo "  --remove       : Remove subsystem."
    echo "  --passwd       : Change root password."
    exit 0
fi
if [[ "$1" == "--fork" ]] ; then
    use_fork="true"
    shift
fi
if [[ "$1" == "--hostctl" ]] ; then
    use_hostctl="true"
    shift
fi
if [[ "$1" == "--system" ]] ; then
    export SYSTEM="$2"
    shift
    shift
fi
source /usr/lib/sulin/dsl/variable.sh
source /usr/lib/sulin/dsl/functions.sh
if [[ "$1" == "--no-install" ]] ; then
    if [[ ! -f "${DESTDIR}/etc/os-release" ]] ; then
        exit 0
    fi
    shift
fi
if [[ "$1" == "" ]] ; then
    exec $(get_root) /usr/lib/sulin/dsl/dsl.sh
fi
if [[ -f "$1" ]] ; then
    cat $1 | exec $(get_root) /usr/lib/sulin/dsl/dsl.sh
fi
if [[ "$1" == "--update" ]] ; then
    isroot
    rm -f ${DESTDIR}/var/cache/app-ltime
    msg "Info" "Check update"
    check_update
    msg "Info" "Unmount all"
    umount_all
    msg "Info" "Sync group id"
    sync_gid
    msg "Info" "Sync applications"
    sync_desktop
    shift
fi
if [[ "$1" == "--umount" ]] ; then
    umount_all
    ps ax | grep -v grep | grep "hostctl" | tr -s ' ' | cut -f "1" -d " " | xargs kill -9 &>/dev/null || true
    rm -f "${DESTDIR}"/run/hostctl
    shift

fi
if [[ "$1" == "--kill" ]] ; then
    isroot
    set +e
    PID_LIST=$(ps aux | grep -v grep | grep dsl.sh | tr -s ' ' | cut -f 2 -d ' ')
    for p in ${PID_LIST[@]} ; do
        msg "Kill" "$p"
        kill -9 $p || true
    done
    pactl unload-module module-native-protocol-tcp &>/dev/null
    rm -f "${DESTDIR}"/run/hostctl
    set -e
    shift
fi
if [[ "$1" == "--remove" ]] ; then
    isroot
    umount_all
    rm -rf "${DESTDIR}"
    rm -rf /usr/share/applications/debian/*.desktop
    shift
fi
if [[ "$1" == "--passwd" ]] ; then
    isroot
    exec chroot "${DESTDIR}" usermod -p $(openssl passwd "$2") root
fi
if [[ "$1" == "-c" ]] ; then
    shift
    if [[ "${use_hostctl}" == "true" ]] && ls ${DESTDIR}/run/hostctl &>/dev/null  ; then
        echo "true" | $(get_root) /usr/lib/sulin/dsl/dsl.sh
        echo 'echo "'$*'" | '$(get_root)' /usr/lib/sulin/dsl/dsl.sh' > ${DESTDIR}/run/hostctl
    elif [[ "${use_fork}" == "true" ]] &>/dev/null  ; then
        echo "$*" | $(get_root) /usr/lib/sulin/dsl/dsl.sh &
    else
        echo "$*" | exec $(get_root) /usr/lib/sulin/dsl/dsl.sh
    fi
fi
